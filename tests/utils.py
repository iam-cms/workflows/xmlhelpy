# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import sys


def _normalize_output(value):
    # Trim all guiding spaces of each line and surrounding newlines of the whole output.
    return "\n".join([line.lstrip() for line in value.split("\n")]).strip("\n")


def check_output(result, expected, exit_code=0):
    """Check the output of a result from running a command."""
    output = _normalize_output(result.output)
    expected = _normalize_output(expected)

    assert result.exit_code == exit_code
    assert output == expected


def check_xml_output(result, expected, exit_code=0):
    """Check the XML output of a result from running the xmlhelp of a command."""
    xml_declaration = f"<?xml version='1.0' encoding='{sys.stdout.encoding}'?>"

    output = _normalize_output(result.output)
    expected = _normalize_output(expected)

    assert result.exit_code == exit_code
    assert output == f"{xml_declaration}\n{expected}"
