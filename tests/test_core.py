# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import click
import pytest

from xmlhelpy import argument
from xmlhelpy import command
from xmlhelpy import environment
from xmlhelpy import option

from .utils import check_output


@pytest.mark.parametrize("cmd_decorator", [command, environment])
def test_parameters(cmd_decorator, runner):
    """Test if different parameters are handled and forwarded correctly."""

    @cmd_decorator()
    # Include a regular argument.
    @argument("arg")
    # Include a regular option with a dash in the name.
    @option("opt-1")
    # Include an option with a custom variable name.
    @option("opt_2", var_name="custom_opt_2", default="opt_2")
    # Include an option marked as flag.
    @option("flag", is_flag=True, default="ignored")
    def func(**kwargs):
        click.echo(sorted(kwargs.items()))

    args = ["arg", "--opt-1", "opt-1"]
    expected = [
        ("arg", "arg"),
        ("opt_1", "opt-1"),
        ("custom_opt_2", "opt_2"),
        ("flag", False),
    ]

    if cmd_decorator == environment:
        args += ["--env-exec", "test"]
        expected += [("env_exec", ("test",))]

    result = runner.invoke(func, args)
    expected = str(sorted(expected))

    check_output(result, expected)


@pytest.mark.parametrize("cmd_decorator", [command, environment])
@pytest.mark.parametrize("is_flag", [False, True])
def test_relations_init_invalid(cmd_decorator, is_flag):
    """Test if invalid relations are handled correctly."""
    with pytest.raises(ValueError) as e:
        cmd_decorator()(
            option("opt_1", is_flag=is_flag, requires=["opt_1"])(lambda: None)
        )

    assert str(e.value) == "Option 'opt_1' cannot require or exclude itself."

    with pytest.raises(ValueError) as e:
        cmd_decorator()(
            option("opt_1", is_flag=is_flag, requires=["opt_2"])(lambda: None)
        )

    assert (
        str(e.value) == "There is no option 'opt_2' that option 'opt_1' can relate to."
    )

    with pytest.raises(ValueError) as e:

        @cmd_decorator()
        @option("opt_1", is_flag=is_flag, excludes=["opt_2"])
        @option("opt_2", default="test")
        def func(**kwargs):
            pass

    assert (
        str(e.value)
        == "Option 'opt_1' excludes option 'opt_2', but at least one of these options"
        " specifies a default value."
    )


@pytest.mark.parametrize("cmd_decorator", [command, environment])
@pytest.mark.parametrize("is_flag", [False, True])
def test_relations_invoke_requires(cmd_decorator, is_flag, runner):
    """Test if requirements are handled correctly when a command is invoked."""

    @cmd_decorator()
    @option("opt_1", is_flag=is_flag)
    @option("opt_2", is_flag=is_flag, requires=["opt_1"])
    def func(**kwargs):
        pass

    # Specify no arguments.
    args = []

    if cmd_decorator == environment:
        args += ["--env-exec", "test"]

    result = runner.invoke(func, args)
    expected = ""

    check_output(result, expected)

    # Specify the first option only.
    if is_flag:
        args = ["--opt_1"]
    else:
        args = ["--opt_1", "test"]

    if cmd_decorator == environment:
        args += ["--env-exec", "test"]

    result = runner.invoke(func, args)
    expected = ""

    check_output(result, expected)

    # Specify the second option only.
    if is_flag:
        args = ["--opt_2"]
    else:
        args = ["--opt_2", "test"]

    if cmd_decorator == environment:
        args += ["--env-exec", "test"]

    result = runner.invoke(func, args)
    expected = "Error: Option 'opt_2' requires option 'opt_1'."

    check_output(result, expected, exit_code=2)


@pytest.mark.parametrize("cmd_decorator", [command, environment])
@pytest.mark.parametrize("is_flag", [False, True])
def test_relations_invoke_excludes(cmd_decorator, is_flag, runner):
    """Test if exclusions are handled correctly when a command is invoked."""

    @cmd_decorator()
    @option("opt_1", is_flag=is_flag)
    @option("opt_2", is_flag=is_flag, excludes=["opt_1"])
    def func(**kwargs):
        pass

    # Specify no arguments.
    args = []

    if cmd_decorator == environment:
        args += ["--env-exec", "test"]

    result = runner.invoke(func, args)
    expected = ""

    check_output(result, expected)

    # Specify the first option only.
    if is_flag:
        args = ["--opt_1"]
    else:
        args = ["--opt_1", "test"]

    if cmd_decorator == environment:
        args += ["--env-exec", "test"]

    result = runner.invoke(func, args)
    expected = ""

    check_output(result, expected)

    # Specify the second option only.
    args = ["--opt_1", "test", "--opt_2", "test"]

    if is_flag:
        args = ["--opt_1", "--opt_2"]
    else:
        args = ["--opt_1", "test", "--opt_2", "test"]

    if cmd_decorator == environment:
        args += ["--env-exec", "test"]

    result = runner.invoke(func, args)
    expected = "Error: Option 'opt_2' excludes option 'opt_1'."

    check_output(result, expected, exit_code=2)
