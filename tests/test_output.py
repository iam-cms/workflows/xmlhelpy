# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from xmlhelpy import Bool
from xmlhelpy import Choice
from xmlhelpy import Float
from xmlhelpy import FloatRange
from xmlhelpy import Integer
from xmlhelpy import IntRange
from xmlhelpy import Path
from xmlhelpy import String
from xmlhelpy import TokenList
from xmlhelpy import argument
from xmlhelpy import command
from xmlhelpy import environment
from xmlhelpy import group
from xmlhelpy import option

from .utils import check_output
from .utils import check_xml_output


def test_commands(runner):
    """Test the "--commands" option."""

    @group()
    def grp():
        pass

    @grp.command()
    def cmd():
        pass

    @grp.environment()
    def env():
        pass

    result = runner.invoke(grp, ["--commands"])
    expected = """
    grp cmd
    grp env
    """

    check_output(result, expected)


@pytest.mark.parametrize("cmd_type", ["command", "environment"])
def test_version(cmd_type, runner):
    """Test the "--version" option."""

    cmd_decorator = globals()[cmd_type]

    @cmd_decorator()
    def func_1():
        pass

    @group(version="1.0.0")
    def grp():
        pass

    cmd_decorator = getattr(grp, cmd_type)

    @cmd_decorator()
    def func_2():
        pass

    @cmd_decorator(version="2.0.0")
    def func_3():
        pass

    expected = ""
    result = runner.invoke(func_1, ["--version"])

    check_output(result, expected)

    expected = "1.0.0"
    result = runner.invoke(grp, ["--version"])

    check_output(result, expected)

    expected = "1.0.0"
    result = runner.invoke(func_2, ["--version"])

    check_output(result, expected)

    expected = "2.0.0"
    result = runner.invoke(func_3, ["--version"])

    check_output(result, expected)


@pytest.mark.parametrize("cmd_type", ["command", "environment"])
def test_xmlhelp_commands_explicit(cmd_type, runner):
    """Test the general "--xmlhelp" option for commands with explicit parameters.

    The command name and description are provided explicitely.
    """

    @group(name="group")
    def grp():
        pass

    cmd_decorator = getattr(grp, cmd_type)

    @cmd_decorator(
        name="test", description="Test description.", example="--test", version="1.0"
    )
    def func():
        pass

    result = runner.invoke(func, ["--xmlhelp"])
    expected = f"""
    <{func.tag} name="group test" description="Test description." example="group test --test" version="1.0"/>
    """

    check_xml_output(result, expected)


@pytest.mark.parametrize("cmd_type", ["command", "environment"])
def test_xmlhelp_commands_implicit(cmd_type, runner):
    """Test the general "--xmlhelp" option for commands without explicit parameters.

    The command name and description are taken implicitely.
    """

    @group()
    def grp():
        pass

    cmd_decorator = getattr(grp, cmd_type)

    @cmd_decorator()
    def func():
        """Test description.

        This is a test description.
        """

    result = runner.invoke(func, ["--xmlhelp"])
    expected = f"""
    <{func.tag} name="grp func" description="Test description."/>
    """

    check_xml_output(result, expected)


@pytest.mark.parametrize("cmd_decorator", [command, environment])
def test_xmlhelp_arguments(cmd_decorator, runner):
    """Test the "--xmlhelp" option for arguments."""

    @cmd_decorator()
    @argument("arg_1", description="Argument 1.")
    @argument("arg_2", nargs=2, default=("a b", "c"))
    @argument("arg_3", required=False)
    @argument("arg_4", exclude_from_xml=True)
    def func(**kwargs):
        pass

    result = runner.invoke(func, ["--xmlhelp"])
    expected = f"""
    <{func.tag} name="func">
        <param description="Argument 1." type="string" name="arg0" positional="true" required="true"/>
        <param type="string" name="arg1" positional="true" required="true" default="a\\ b c" nargs="2"/>
        <param type="string" name="arg2" positional="true"/>
    </{func.tag}>
    """

    check_xml_output(result, expected)


@pytest.mark.parametrize("cmd_decorator", [command, environment])
def test_xmlhelp_options(cmd_decorator, runner):
    """Test the "--xmlhelp" option for options."""

    @cmd_decorator()
    @option("opt_1", description="Option 1.")
    @option("opt_2", param_type=Bool, nargs=2, default=(True, False))
    @option("opt_3", required=True)
    @option("opt_4", char="o")
    @option("opt_5", var_name="custom_opt_5")
    @option("opt_6", is_flag=True)
    @option("opt_7", requires=["opt_6", "opt_8"])
    @option("opt_8", excludes=["opt_9"])
    @option("opt_9", exclude_from_xml=True)
    def func(**kwargs):
        pass

    result = runner.invoke(func, ["--xmlhelp"])
    expected = f"""
    <{func.tag} name="func">
        <param description="Option 1." type="string" name="opt_1"/>
        <param type="bool" name="opt_2" default="true false" nargs="2"/>
        <param type="string" name="opt_3" required="true"/>
        <param type="string" name="opt_4" char="o"/>
        <param type="string" name="opt_5"/>
        <param type="flag" name="opt_6" default="false"/>
        <param type="string" name="opt_7" relations="opt_6|opt_8"/>
        <param type="string" name="opt_8" relations="!opt_9"/>
    </{func.tag}>
    """

    check_xml_output(result, expected)


@pytest.mark.parametrize("cmd_decorator", [command, environment])
def test_xmlhelp_types(cmd_decorator, runner):
    """Test the "--xmlhelp" option for all parameter types."""

    @cmd_decorator()
    @option("opt_1", param_type=String, default="test")
    @option("opt_2", param_type=TokenList(separator=";"), default="a;b")
    @option("opt_3", param_type=Bool, default=True)
    @option("opt_4", param_type=Bool, default=False)
    @option("opt_5", param_type=Integer, default=1)
    @option("opt_6", param_type=IntRange(min=-1, max=1), default=1)
    @option("opt_7", param_type=Float, default=0.5)
    @option("opt_8", param_type=FloatRange(min=-0.5, max=0.5), default=0.5)
    @option("opt_9", param_type=Choice(choices=["a", "b|c"]), default="a")
    @option("opt_10", param_type=Path(exists=True), default="/foo")
    @option("opt_11", param_type=Path(path_type="directory"))
    @option("opt_12", param_type=Path(path_type="file"))
    def func(**kwargs):
        pass

    result = runner.invoke(func, ["--xmlhelp"])
    expected = f"""
    <{func.tag} name="func">
        <param type="string" name="opt_1" default="test"/>
        <param type="tokenlist" separator=";" name="opt_2" default="a;b"/>
        <param type="bool" name="opt_3" default="true"/>
        <param type="bool" name="opt_4" default="false"/>
        <param type="long" name="opt_5" default="1"/>
        <param type="long_range" min="-1" max="1" name="opt_6" default="1"/>
        <param type="real" name="opt_7" default="0.5"/>
        <param type="real_range" min="-0.5" max="0.5" name="opt_8" default="0.5"/>
        <param type="choice" choices="a|b\\|c" case_sensitive="false" name="opt_9" default="a"/>
        <param type="path" exists="true" name="opt_10" default="/foo"/>
        <param type="directory" exists="false" name="opt_11"/>
        <param type="file" exists="false" name="opt_12"/>
    </{func.tag}>
    """

    check_xml_output(result, expected)


def test_help(runner):
    """Test the "--help" option."""

    @group()
    def grp():
        """Test description.

        This is a test description.
        """

    @grp.command(example="--help")
    def cmd():
        """Test description.

        This is a test description.
        """

    @grp.environment(example="--help")
    def env():
        """Test description.

        This is a test description.
        """

    result = runner.invoke(grp, ["--help"])
    expected = """
    Usage: grp [OPTIONS] COMMAND [ARGS]...

      Test description.

      This is a test description.

    Options:
      --version   Print the version of this group and exit.
      --commands  Print a list of all commands this group contains and exit.
      --help      Show this message and exit.

    Commands:
      cmd  Test description.
      env  Test description.
    """

    check_output(result, expected)

    result = runner.invoke(grp, ["cmd", "--help"])
    expected = """
    Usage: grp cmd [OPTIONS]
    Example: grp cmd --help

    Test description.

    This is a test description.

    Options:
    --version  Print the version of this command and exit.
    --xmlhelp  Print the xmlhelp of this command and exit.
    --help     Show this message and exit.
    """

    check_output(result, expected)

    result = runner.invoke(grp, ["env", "--help"])
    expected = """
    Usage: grp env [OPTIONS]
    Example: grp env --help

    Test description.

    This is a test description.

    Options:
    --version        Print the version of this command and exit.
    --xmlhelp        Print the xmlhelp of this command and exit.
    --env-exec TEXT  Command string to be executed inside the environment.
    [required]
    --help           Show this message and exit.
    """

    check_output(result, expected)
