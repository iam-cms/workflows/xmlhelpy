# Release history

## 0.14.0 (2024-10-23)

* Dropped support for Python 3.8 and added support for Python 3.13.

## 0.13.0 (2023-10-04)

* Added support for Python 3.12.

## 0.12.0 (2023-09-07)

* Dropped support for Python 3.7.
* Removed the `__version__` attribute in favor of using
  `importlib.metadata.version("xmlhelpy")`.

## 0.11.0 (2023-06-14)

* Added support for Python 3.11.

## 0.10.1 (2022-12-05)

* Fixed an issue where the encoding defined in the declaration of the XML
  output does not match the encoding of the current terminal.

## 0.10.0 (2022-11-04)

* Removed support for Python 3.6.

## 0.9.2 (2022-02-04)

* Added `AUTHORS.md`.

## 0.9.1 (2021-12-01)

* Fixed requirement checks being triggered in some cases even if none of the
  options to check were specified.

## 0.9.0 (2021-11-30)

* Removed the behavior of relations always being set in both directions
  implicitely, which allows for more flexibility in some cases.

## 0.8.3 (2021-11-25)

* Fixed another case where excluded flag options were not working properly.

## 0.8.2 (2021-11-24)

* Relaxed required Python version again.

## 0.8.1 (2021-11-19)

* Fixed flag options not working properly when being involved in exclusions.

## 0.8.0 (2021-11-02)

* Fixed parsing of default values for token lists.
* Added the `requires` and `excludes` parameters to options to specify
  relations to other options.
* Fixed custom variable names being used in the xmlhelp instead of the
  parameter names.
* Added an additional `positional` attribute in the xmlhelp to distinguish
  arguments and options.
* Changed the possible `path_type` values of the `Path` parameter type to
  `directory` and `file`.
* Fixed the path types of the `Path` parameter type not being used in the
  xmlhelp.
* Added tests for most custom handling and behavior that is not part of Click.
* Replaced the example script with a proper documentation.

## 0.7.0 (2021-10-08)

* Made the `env-exec` option for environments required.
* Added compatibility with Click>=8.0.
* Added an `exclude_from_xml` flag to exclude arguments and options from the
  xmlhelp output.

## 0.6.1 (2021-07-08)

* Fixed missing parameter descriptions in help output.

## 0.6.0 (2021-07-06)

* Removed the `as_string` option for all types.
* Added a new type for token lists.
* Only the first line of the command docstring is now taken as a fallback
  description in the xmlhelp.

## 0.5.0 (2021-05-26)

* Added a new class and decorator for environments.

## 0.4.0 (2021-03-23)

* Added the possibility to use custom command classes in decorators.
* Excluded empty examples in the xmlhelp.

## 0.3.0 (2021-02-25)

* Added example usage to help output.
* Added validation for duplicate parameter names and chars.

## 0.2.0 (2021-01-27)

* Added a `--commands` option to list all commands of a group.
* Added the option to pass a version to groups as well, which commands that do
  not specify their own version will inherit.
* Added a `--version` option to get the version of a group or command.

## 0.1.0 (2021-01-14)

* Initial beta release.
