# Authors

Currently maintained by the **Kadi4Mat Team**.

List of contributions from the Kadi4Mat team and other contributors, ordered by
date of first contribution:

* **Nico Brandt**
* **Lars Griem**
* **Ephraim Schoof**
